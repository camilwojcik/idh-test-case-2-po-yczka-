<?php


namespace objects;

use DateInterval;
use DatePeriod;
use DateTime;
use Exception;

class Loan
{
    private $loanDate;
    private $amount;
    private $commision;

    /**@var $payments Payment[]*/
    private $payments = [];

    const INTEREST_RATE = 0.45;

    /**
     * Loan constructor.
     * @param $loanDate
     * @param $amount
     * @param $commision
     * @throws Exception
     */
    public function __construct( DateTime $loanDate,  float $amount, float $commision)
    {
        $this->loanDate = $loanDate;
        $this->amount = $amount;
        $this->commision = $commision;
    }

    /**
     * Adds a payment into array of payments in the object
     * @param Payment $payment
     * @throws Exception - when overpaid loan
     */
    public function addPayment(Payment $payment) : void
    {
        $leftToPay = $this->getTotal($payment->getPaymentDate(), $payment);
        if($leftToPay>=0){
            $this->payments[] = $payment;
        }else{
            $overPaid = abs($leftToPay);
            throw new Exception('Payment refused! Unable to overpay your loan!');
            //Set as try catch and put into class maintaining refunds? w/ $overPaid variable in construct
//            $payment = new Payment(($payment->getAmount() - $leftToPay), $payment->getPaymentDate());
//            $this->payments[] = $payment;
        }
    }

    /**
     * @return Payment[]
     */
    public function getPayments(): array
    {
        return $this->payments;
    }

    /**
     * Returns loan`s amount left to pay at given date
     * @param DateTime $date default today`s
     * @param Payment|null $testPayment - "temporary count total with additional payment to check if not overpaid
     * @return float
     * @throws Exception - when dateTime is invalid
     */
    public function getTotal(DateTime $date = null, Payment $testPayment = null) : float
    {
        //Get date periods to foreach to
        $searchDate = ($date instanceof DateTime) ? clone $date : new DateTime();
        $searchDate->modify('+1 day');
        $datePeriod = new DatePeriod($this->loanDate, DateInterval::createFromDateString('1 day'), $searchDate, DatePeriod::EXCLUDE_START_DATE);

        //Get interesting fields into local variables
        $interest = 0;
        $commission = $this->commision;
        $amount = $this->amount;
        $overpaid = 0;
        foreach ($datePeriod as $day){
            //not counting interest rate when loan is already paid
            if($amount>0){
                $interest += self::INTEREST_RATE;
                $payments = $this->getPayments();
                if(!empty($testPayment))
                    $payments[] = clone $testPayment;
                foreach ($payments as $k => $payment){
                    if($payment->getPaymentDate()->format('Y-m-d') == $day->format('Y-m-d')){
                        $interest = $payment->reduceBy($interest);
                        $commission = $payment->reduceBy($commission);
                        $amount = $payment->reduceBy($amount);
                        $overpaid+=$payment->getAmount();
                        unset($payments[$k]);
                    }
                }
            }
        }
        return ($interest+$commission+$amount)-$overpaid;
    }

}