<?php

namespace objects;

use DateTime;
use Exception;

class Payment
{
    private $paymentDate;
    private $amount;

    /**
     * Payment constructor.
     * @param float $amount
     * @param DateTime $paymentDate
     * @throws Exception
     */
    public function __construct(float $amount, DateTime $paymentDate = null)
    {
        $this->amount = $amount;
        $this->paymentDate = $paymentDate;
    }

    /**
     * @return DateTime
     */
    public function getPaymentDate(): DateTime
    {
        return $this->paymentDate;
    }

    public function getAmount() : float {
        return $this->amount;
    }

    /**
     * Reduces field amount by specified amount and returns leftover amount ( left to pay )
     * @param float $amount - amount to reduce by
     * @return float
     */
    public function reduceBy(float $amount) : float
    {
        if($amount<$this->amount){
            $this->amount -= $amount;
            $amount=0;
        }else{
            $amount = $amount - $this->amount;
            $this->amount=0;
        }
        return $amount;
    }

}