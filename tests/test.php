<?php

use objects\Loan;
use objects\Payment;

include "../vendor/autoload.php";
include "../src/objects/Loan.php";
include "../src/objects/Payment.php";


class testCase extends PHPUnit\Framework\TestCase {

    /**Tests loan and interest rate counting
     * @throws Exception
     */
    public function testCase1()
    {
        $testDate = new DateTime('1998-10-05');
        $testDate2 = new DateTime('1998-10-07');
        $loan = new Loan($testDate, 1700, 466.65);
        $this->assertEquals(2167.55, $loan->getTotal($testDate2));
    }

    /**Tests loan with single payment
     * @throws Exception
     */
    public function testCase2()
    {
        $testDate = new DateTime('1998-10-05');
        $testDate2 = new DateTime('1998-10-08');
        $testDate3 = new DateTime('1998-10-08');

        $loan = new Loan($testDate, 1700, 466.65);
        $payment = new Payment(1000, $testDate2);
        $loan->addPayment($payment);
        $this->assertEquals(1168, $loan->getTotal($testDate3));
    }

    /**Tests loan with over payment v1
     * @throws Exception
     */
    public function testCase3()
    {
        $testDate = new DateTime('1998-10-05');
        $testDate2 = new DateTime('1998-10-08');

        try{
            $loan = new Loan($testDate, 1700, 466.65);
            $payment = new Payment(100000, $testDate2);
            $loan->addPayment($payment);
        }catch (Exception $exception){
            $this->assertEquals('Payment refused! Unable to overpay your loan!', $exception->getMessage());
        }
    }

    /**Tests loan with over payment v2
     * @throws Exception
     */
    public function testCase4()
    {
        $testDate = new DateTime('1998-10-05');
        $testDate2 = new DateTime('1998-10-08');
        $loan = new Loan($testDate, 1700, 466.65);
        $payment = new Payment(100000, $testDate2);
        $this->expectException(Exception::class);
        $loan->addPayment($payment);
    }
}